# duniter-mini-client

A simple Duniter client written in Rust.

Just for training with Rust.

## Requirements

- `sudo apt install libssl-dev`

## Build

    cargo build

## Usage

### Target node

    duniter-mini-client lookup cgeek

By default, mini-client will target `https://g1-test.duniter.org`. To use another node, change `DUNITER_NODE` environment variable :

    DUNITER_NODE=https://g1.cgeek.fr duniter-mini-client lookup cgeek

Or : 

    export DUNITER_NODE=https://g1.cgeek.fr

### Commands

#### `lookup`

Performs a BMA /wot/lookup HTTP request to find identities both on mempool and blockchain.

    duniter-mini-client lookup cgeek

#### `certify`

> Requires a keyring file (see Options > Keyring section below)

Certify and identity by creating the certification and submitting it to the network.

    duniter-mini-client certify cgeek

#### `adhere`

> Requires a keyring file (see Options > Keyring section below)

Adhere to or renew your membership to the currency.

    duniter-mini-client adhere cgeek

## Options

The client uses environment variables to set specific options:

* `DUNITER_NODE`: the network node (with BMA enabled) which will receive the HTTP requests
* `DUNITER_KEYRING_PATH`: path to the keyring generated using `duniter-mini-client keyring <salt> <passwd>` command