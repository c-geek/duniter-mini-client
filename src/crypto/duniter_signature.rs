use std::fmt::Formatter;

const SIGNATURE_LENGTH: usize = 64;

pub struct DuniterSignature([u8; SIGNATURE_LENGTH]);

/// A DuniterSignature wraps an actuel signature (64 bytes data)
impl DuniterSignature {
    pub fn new(signature: [u8; SIGNATURE_LENGTH]) -> Self {
        Self(signature)
    }
    pub fn as_bytes(&self) -> [u8; SIGNATURE_LENGTH] {
        self.0
    }
}

impl std::fmt::Display for DuniterSignature {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", base64::encode(self.0))
    }
}