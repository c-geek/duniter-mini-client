use std::fs;
use std::path::{PathBuf};

use crate::crypto::duniter_key::{DuniterKey, PUBLIC_KEY_LEN, SECRET_KEY_LEN, ToDuniterKey};

pub struct FileBase58DuniterKey(PathBuf);

impl From<PathBuf> for FileBase58DuniterKey {
    fn from(path_buf: PathBuf) -> Self {
        FileBase58DuniterKey(path_buf)
    }
}

impl ToDuniterKey for FileBase58DuniterKey {

    fn derive(&self) -> DuniterKey {
        let content = fs::read_to_string(&self.0)
            .map(|s| s.replace("\n", ""))
            .map(|s| s.split(":")
                .map(str::to_owned).collect::<Vec<_>>().into_iter());
        if let Ok(mut split) = content {
            let public_vec = bs58::decode(split.next().expect("Should have read the public key")).into_vec().unwrap();
            let secret_vec = bs58::decode(split.next().expect("Should have read the private key")).into_vec().unwrap();
            let mut public = [0u8; PUBLIC_KEY_LEN];
            let mut secret = [0u8; SECRET_KEY_LEN];
            for i in 0..PUBLIC_KEY_LEN { public[i] = public_vec[i] }
            for i in 0..SECRET_KEY_LEN { secret[i] = secret_vec[i] }
            return DuniterKey { public, secret }
        }
        panic!("Could not extract base58 keyring from path {}", self.0.display());
    }
}

#[cfg(test)]
mod tests {
    use std::io::Write;

    use crate::crypto::duniter_key::file_base58_duniter_key::FileBase58DuniterKey;
    use crate::crypto::duniter_key::ToDuniterKey;

    const PUBKEY: &str = "953SE7QJoDC2vFFwkSDojGKjUFU6BCu1kH6s4MnoyQCw";
    const SECKEY: &str = "s3G7v2FPuTSWh8f9BSrU8EurwcM214x4fMLWFDLYmH7doqfyCqM96Ai5q4xB8y4GBKTCUvHoR3Et8AJKf7XTTZR";

    #[test]
    fn load_from_file() {
        let file = tempfile::NamedTempFile::new().unwrap();
        writeln!(&file, "{}:{}", PUBKEY, SECKEY).expect("Could not write temporary key");
        let path = &file.into_temp_path();
        let key = FileBase58DuniterKey(path.to_path_buf()).derive();
        assert_eq!(PUBKEY, key.get_public_base58());
        assert_eq!(SECKEY, key.get_secret_base58());
    }
}