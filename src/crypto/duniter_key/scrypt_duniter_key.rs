use crate::crypto::duniter_key::{ToDuniterKey, DuniterKey};
use base64;
use cryptoxide::ed25519;
use std::error::Error;
use scrypt::password_hash::{Output, SaltString, PasswordHasher};
use scrypt::{Params, Scrypt};

const SCRYPT_N: u8 = 12;
const SCRYPT_R: u32 = 16;
const SCRYPT_P: u32 = 1;

pub struct ScryptDuniterKey((String, String));

impl ScryptDuniterKey {
    pub fn new(salt: String, passwd: String) -> Self {
        Self((salt, passwd))
    }
}

impl From<ScryptDuniterKey> for DuniterKey {
    fn from(sdk: ScryptDuniterKey) -> Self {
        sdk.derive()
    }
}

impl ToDuniterKey for ScryptDuniterKey {

    fn derive(&self) -> DuniterKey {
        let (salt, passwd) = &self.0;
        let seed = derive_seed(salt.as_str(), passwd.as_str()).expect("Could not derive seed from salt/password.");
        let (secret, public) = ed25519::keypair(seed.as_bytes());
        DuniterKey { public, secret }
    }
}

fn derive_seed(salt: &str, passwd: &str) -> Result<Output, Box<dyn Error>> {
    let saltb64 = base64::encode(salt); // convert utf8 to base64
    let salt = SaltString::new(saltb64.as_str())?; // expectes base64-encoded str
    let params = Params::new(SCRYPT_N, SCRYPT_R, SCRYPT_P)?;
    let password_hash = Scrypt.hash_password(passwd.as_bytes(), None, params, &salt)?;
    Ok(password_hash.hash.expect("Seed should have been generated"))
}