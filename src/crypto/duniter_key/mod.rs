use std::fmt::Formatter;

use bs58;
use cryptoxide::ed25519;

pub use scrypt_duniter_key::ScryptDuniterKey;
pub use seed_duniter_key::SeedDuniterKey;

use crate::crypto::duniter_signature::DuniterSignature;
use crate::dubp::signable::Signable;

/// The implementations
pub mod scrypt_duniter_key;
pub mod seed_duniter_key;
pub mod base58_duniter_key;
pub mod file_base58_duniter_key;

pub const PUBLIC_KEY_LEN: usize = 32;
pub const SECRET_KEY_LEN: usize = 64;

/// The main functional class for handle Duniter crypto.
pub struct DuniterKey {
    pub public: [u8; PUBLIC_KEY_LEN],
    pub secret: [u8; SECRET_KEY_LEN],
}

pub trait ToDuniterKey {
    fn derive(&self) -> DuniterKey;
}

/// How to display a duniter keyring: "pub_base58:sec_base58" format.
impl std::fmt::Display for DuniterKey {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}:{}", bs58::encode(&self.public).into_string(), bs58::encode(&self.secret).into_string())
    }
}

impl DuniterKey {

    pub fn get_public_base58(&self) -> String {
        bs58::encode(self.public).into_string()
    }

    pub fn get_secret_base58(&self) -> String {
        bs58::encode(self.secret).into_string()
    }

    pub fn sign(&self, message: &dyn Signable) -> DuniterSignature {
        DuniterSignature::new(ed25519::signature(message.to_signable().as_bytes(), &self.secret[..]))
    }

    pub fn verify(&self, message: &str, signature: &DuniterSignature) -> bool {
        ed25519::verify(message.as_bytes(), &self.public, &signature.as_bytes())
    }
}
