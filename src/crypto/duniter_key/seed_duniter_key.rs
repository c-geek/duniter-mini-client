use crate::crypto::duniter_key::{ToDuniterKey, DuniterKey};
use base64;
use cryptoxide::ed25519;

const SEED_LEN: usize = 32;

pub struct SeedDuniterKey(String);

impl SeedDuniterKey {
    pub fn new(seed: String) -> Self {
        Self(seed)
    }
}

impl From<SeedDuniterKey> for DuniterKey {
    fn from(sdk: SeedDuniterKey) -> Self {
        sdk.derive()
    }
}

impl ToDuniterKey for SeedDuniterKey {

    fn derive(&self) -> DuniterKey {
        let seed = &self.0;
        let seed_vec = base64::decode(seed).unwrap();
        let mut seed = [0u8; SEED_LEN];
        for i in 0..SEED_LEN { seed[i] = seed_vec[i] }
        let (secret, public) = ed25519::keypair(&seed);
        DuniterKey { public, secret }
    }
}