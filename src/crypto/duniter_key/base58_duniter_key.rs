use crate::crypto::duniter_key::{ToDuniterKey, DuniterKey, PUBLIC_KEY_LEN, SECRET_KEY_LEN};

pub struct Base58DuniterKey(String, String);

impl Base58DuniterKey {
    pub fn new(public: String, secret: String) -> Self {
        Self(public, secret)
    }
}

impl From<Base58DuniterKey> for DuniterKey {
    fn from(sdk: Base58DuniterKey) -> Self {
        sdk.derive()
    }
}

impl ToDuniterKey for Base58DuniterKey {

    fn derive(&self) -> DuniterKey {
        let public_vec = bs58::decode(&self.0).into_vec().unwrap();
        let secret_vec = bs58::decode(&self.1).into_vec().unwrap();
        let mut public = [0u8; PUBLIC_KEY_LEN];
        let mut secret = [0u8; SECRET_KEY_LEN];
        for i in 0..PUBLIC_KEY_LEN { public[i] = public_vec[i] }
        for i in 0..SECRET_KEY_LEN { secret[i] = secret_vec[i] }
        DuniterKey { public, secret }
    }
}