pub mod crypto;
pub mod dubp;
pub mod cli;
pub mod bma;

use crate::crypto::duniter_key::{ToDuniterKey};

pub fn compute_pub(sdk: impl ToDuniterKey) -> String {
    sdk.derive().get_public_base58()
}

pub fn compute_sec(sdk: impl ToDuniterKey) -> String {
    sdk.derive().get_secret_base58()
}

pub fn compute_key(sdk: impl ToDuniterKey) -> String {
    format!("{}", sdk.derive())
}

#[cfg(test)]
mod tests {

    use crate::*;
    use crate::crypto::duniter_key::SeedDuniterKey;
    use crate::crypto::duniter_key::base58_duniter_key::Base58DuniterKey;
    use crate::crypto::duniter_key::scrypt_duniter_key::{ScryptDuniterKey};
    use crate::dubp::signable::Signable;

    const SALT: &str = "test_salt";
    const PASSWD: &str = "test_passwd";
    const PUBKEY: &str = "953SE7QJoDC2vFFwkSDojGKjUFU6BCu1kH6s4MnoyQCw";
    const SECKEY: &str = "s3G7v2FPuTSWh8f9BSrU8EurwcM214x4fMLWFDLYmH7doqfyCqM96Ai5q4xB8y4GBKTCUvHoR3Et8AJKf7XTTZR";
    const SEED: &str = "KybWwsHnqJrs+1F10GfuK08pOyvqpb2Jn1yNO58S0l0=";

    impl Signable for String {
        fn to_signable(self: &Self) -> String {
            self.clone()
        }
    }

    #[test]
    fn should_compute_pub() {
        assert_eq!(compute_pub(SeedDuniterKey::new(String::from(SEED))), PUBKEY);
    }

    #[test]
    fn should_compute_sec() {
        assert_eq!(compute_sec(SeedDuniterKey::new(String::from(SEED))), SECKEY);
    }

    #[test]
    fn should_compute_keyring() {
        assert_eq!(compute_key(SeedDuniterKey::new(String::from(SEED))), format!("{}:{}", PUBKEY, SECKEY));
    }

    #[test]
    fn all_derivations_leads_to_the_same() {
        let scrypt_key = ScryptDuniterKey::new(String::from(SALT), String::from(PASSWD)).derive();
        let bs58_key = Base58DuniterKey::new(String::from(PUBKEY), String::from(SECKEY)).derive();
        let seed_key = SeedDuniterKey::new(String::from(SEED)).derive();
        assert_eq!(bs58_key.public, scrypt_key.public);
        assert_eq!(bs58_key.secret, scrypt_key.secret);
        assert_eq!(seed_key.public, scrypt_key.public);
        assert_eq!(seed_key.secret, scrypt_key.secret);
    }

    #[test]
    fn should_verify_or_reject_sig() {
        let bs58_key = Base58DuniterKey::new(String::from(PUBKEY), String::from(SECKEY)).derive();
        let key = bs58_key;
        let signature = key.sign(&"GOOD MESSAGE".to_string());
        assert_eq!(true, key.verify("GOOD MESSAGE", &signature));
        assert_eq!(false, key.verify("WRONG MESSAGE", &signature));
    }
}