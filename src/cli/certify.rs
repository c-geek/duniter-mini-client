use crate::bma::BmaNode;
use crate::bma::lookup_identity::{LookupResult, lookup2identities, lookup_print};
use crate::crypto::duniter_key::ToDuniterKey;
use crate::dubp::documents::certification::Certification;
use crate::dubp::signable::Signable;

pub fn certify(keyring: &Box<dyn ToDuniterKey>, node: &BmaNode, uid_or_pub: &String) -> Result<(), String> {
    let address = node.get_address();
    println!("Fetching identity using \"{}\" pattern...", uid_or_pub);
    let resp = reqwest::blocking::get(format!("{}/wot/lookup/{}", address, uid_or_pub)).expect("Could not fetch lookup data from distant node");
    let res_json = &resp.json().expect("Could not get JSON result from distant node");
    let res_json: LookupResult = LookupResult(res_json);
    let results = lookup2identities(&res_json);
    let idty = match results.len() {
        0 => return Err(String::from("No matching identity found")),
        1 => &results[0],
        _ => {
            lookup_print(&res_json);
            return Err(format!("Too much identities found ({})", results.len()))
        },
    };

    if idty.revoked {
        return Err(format!("Identity {} {} is revoked and cannot be certified anymore.", idty.public, idty.uid))
    }

    println!("Deriving key for signature...");
    let keyring = keyring.derive();
    println!("Get current block for blockstamp...");
    let current = crate::bma::blockchain::current(&node);
    let currency = &current.currency;
    let issuer = keyring.get_public_base58();
    let current_blockstamp = &current.blockstamp();

    let certification = Certification {
        version: 10,
        currency: currency.clone(),
        issuer,
        idty_issuer: (&idty).public.clone(),
        idty_uid: (&idty).uid.clone(),
        idty_blockstamp: (&idty).blockstamp.clone(),
        idty_sig: (&idty).signature.clone(),
        cert_blockstamp: current_blockstamp.clone(),
    };
    let signature = keyring.sign(&certification);
    let result = crate::bma::wot::certify(&node, &certification.to_signable(), &signature.to_string());
    if let Ok(_) = result {
        println!("Certification sent successfully.");
    } else {
        eprintln!("An error occured during certification.");
    }
    result
}