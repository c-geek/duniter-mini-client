use std::env;
use std::path::PathBuf;

use crate::{bma, cli, compute_key, compute_pub, compute_sec};
use crate::bma::BmaNode;
use crate::cli::Command::*;
use crate::crypto::duniter_key::{ScryptDuniterKey, ToDuniterKey};
use crate::crypto::duniter_key::file_base58_duniter_key::FileBase58DuniterKey;

pub mod certify;
pub mod adhere;

const DEFAULT_NODE: &str = "https://g1-test.duniter.org";
const DEFAULT_KEYRING_PATH: &str = "keyring-base58.txt";
const CMD_PUB: &str = "pub";
const CMD_SEC: &str = "sec";
const CMD_KEYRING: &str = "keyring";
const CMD_LOOKUP: &str = "lookup";
const CMD_CERTIFY: &str = "certify";
const CMD_ADHERE: &str = "adhere";

pub enum Command {
    /// Compute the public key from salt/passwd and displays it
    PUB(String, String, String),
    /// Compute the secret key from salt/passwd and displays it
    SEC(String, String, String),
    /// Compute the keyring from salt/passwd and displays it
    KEYRING(String, String, String),
    /// Search an identity on a Duniter node (BMA API)
    LOOKUP(String, BmaNode, String),
    /// Search an identity on a Duniter node (BMA API) and certify it
    CERTIFY(String, Box<dyn ToDuniterKey>, BmaNode, String),
    /// Search an identity matching local pubkey on a Duniter node (BMA API) and send a membership based on it
    ADHERE(String, Box<dyn ToDuniterKey>, BmaNode),
    /// Some unknown command
    UNKNOWN(String),
}

impl Command {

    pub fn name(&self) -> String {
        let str_name = match self {
            PUB(..) => CMD_PUB,
            SEC(..) => CMD_SEC,
            KEYRING(..) => CMD_KEYRING,
            LOOKUP(..) => CMD_LOOKUP,
            CERTIFY(..) => CMD_CERTIFY,
            ADHERE(..) => CMD_ADHERE,
            UNKNOWN(name) => name.as_str(),
        };
        str_name.to_string()
    }

    pub fn execute(&self) -> Result<(), String> {
        match self {
            PUB(_, salt, passwd) => {
                println!("{}", compute_pub(ScryptDuniterKey::new(salt.to_string(), passwd.to_string())));
            }
            SEC(_, salt, passwd) => println!("{}", compute_sec(ScryptDuniterKey::new(salt.to_string(), passwd.to_string()))),
            KEYRING(_, salt, passwd) => println!("{}", compute_key(ScryptDuniterKey::new(salt.to_string(), passwd.to_string()))),
            LOOKUP(_, bma_node, uid_or_pub) => bma::lookup_identity::lookup(bma_node, uid_or_pub),
            CERTIFY(_, keyring, bma_node, uid_or_pub) => cli::certify::certify(keyring, bma_node, uid_or_pub).unwrap_or_else(|e| eprintln!("{}", e)),
            ADHERE(_, keyring, bma_node) => cli::adhere::adhere(keyring, bma_node).unwrap_or_else(|e| eprintln!("{}", e)),
            UNKNOWN(cmd) => eprintln!("Unknown command {}", cmd),
        }
        Ok(())
    }

    pub fn from<I>(mut args: I) -> Option<Command>
    where I:Iterator<Item = String> {
        args.next();
        let command = match args.next() {
            None => return None,
            Some(cmd_name) => cmd_name
        };

        let command = command.as_str();
        let bma_node = BmaNode::new(env::var("DUNITER_NODE").unwrap_or(DEFAULT_NODE.to_string()).as_str());
        let keyring = Box::from(
            FileBase58DuniterKey::from(
                PathBuf::from(
                    env::var("DUNITER_KEYRING_PATH").unwrap_or(DEFAULT_KEYRING_PATH.to_string()))));
        match command {
            CMD_PUB => {
                let salt = args.next().expect("Salt must be provided");
                let passwd = args.next().expect("Password must be provided");
                Some(PUB(command.to_string(), salt, passwd))
            },
            CMD_SEC => {
                let salt = args.next().expect("Salt must be provided");
                let passwd = args.next().expect("Password must be provided");
                Some(SEC(command.to_string(), salt, passwd))
            },
            CMD_KEYRING => {
                let salt = args.next().expect("Salt must be provided");
                let passwd = args.next().expect("Password must be provided");
                Some(KEYRING(command.to_string(), salt, passwd))
            },
            CMD_LOOKUP => {
                let uid_or_pub = args.next().expect("UID or pubkey must be provided");
                Some(LOOKUP(command.to_string(), bma_node, uid_or_pub))
            },
            CMD_CERTIFY => {
                let uid_or_pub = args.next().expect("UID or pubkey must be provided");
                Some(CERTIFY(command.to_string(), keyring, bma_node, uid_or_pub))
            },
            CMD_ADHERE => {
                Some(ADHERE(command.to_string(), keyring, bma_node))
            },
            _ => Some(UNKNOWN(command.to_string())),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::cli::Command;

    #[test]
    fn known_command () {
        let args = vec!["", "pub", "abc", "def"];
        let cmd = Command::from(args.into_iter().map(|x| x.to_string()));
        assert!(cmd.is_some());
        let cmd = cmd.unwrap();
        let is_expected_cmd = if let Command::PUB(..) = cmd { true } else { false };
        assert!(is_expected_cmd);
    }

    #[test]
    fn unknown_command () {
        let args = vec!["", "abracadabra"];
        let cmd = Command::from(args.into_iter().map(|x| x.to_string()));
        assert!(cmd.is_some());
        let cmd = cmd.unwrap();
        let is_expected_cmd = if let Command::UNKNOWN(..) = cmd { true } else { false };
        assert!(is_expected_cmd);
    }

    #[test]
    fn no_command () {
        let args = vec![""];
        let cmd = Command::from(args.into_iter().map(|x| x.to_string()));
        assert!(cmd.is_none());
    }
}