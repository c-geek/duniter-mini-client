use serde_json::Value;

use crate::bma::BmaNode;

pub struct Block<> {
    pub version: u64,
    pub currency: String,
    pub number: u64,
    pub hash: String
}

impl Block {

    pub fn from_json(json: &Value) -> Block {
        Block {
            currency: json["currency"].as_str().unwrap().into(),
            version: json["version"].as_u64().unwrap(),
            number: json["number"].as_u64().unwrap(),
            hash: json["hash"].as_str().unwrap().into()
        }
    }

    pub fn blockstamp(&self) -> String {
        format!("{}-{}", self.number, self.hash)
    }
}

pub fn current(node: &BmaNode) -> Block {
    let address = node.get_address();
    let resp = reqwest::blocking::get(format!("{}/blockchain/current", address)).expect("Could not fetch /blockchain/current from distant node");
    let json = &resp.json().expect("Could not parse /blockchain/current JSON response");
    Block::from_json(&json)
}