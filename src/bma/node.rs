pub struct BmaNode(String);

impl BmaNode {

    pub fn new(address: &str) -> BmaNode {
        BmaNode(String::from(address))
    }

    pub fn get_address(&self) -> &str {
        &self.0
    }
}