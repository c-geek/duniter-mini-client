use crate::bma::{BmaNode};
use serde_json::Value;

pub struct LookupResult<'a>(pub(crate) &'a serde_json::Value);

pub struct LookupIdentity {
    pub public: String,
    pub uid: String,
    pub blockstamp: String,
    pub signature: String,
    pub revoked: bool
}

impl LookupIdentity {
    pub fn new(public: &str, uid: &str, blockstamp: &str, signature: &str, revoked: bool) -> LookupIdentity {
        LookupIdentity {
            public: String::from(public),
            uid: String::from(uid),
            blockstamp: String::from(blockstamp),
            signature: String::from(signature),
            revoked
        }
    }
}

pub fn lookup(node: &BmaNode, uid_or_pub: &String) -> () {
    let address = node.get_address();
    let resp = reqwest::blocking::get(format!("{}/wot/lookup/{}", address, uid_or_pub)).expect("Could not fetch lookup data from distant node");
    lookup_print(&LookupResult(&resp.json().expect("Could not get JSON result from distant node")));
}

pub fn lookup_print(lookup_result: &LookupResult) -> () {
    lookup2identities(lookup_result).iter().for_each(|i| println!("{} {} {}", i.public, i.uid, if i.revoked { "revoked" } else { "valid" }))
}

pub fn lookup2identities(lookup_result: &LookupResult) -> Vec<LookupIdentity> {
    let mut identities: Vec<LookupIdentity> = vec![];
    for result in lookup_result.0["results"].as_array().unwrap() {
        let pubkey = result["pubkey"].as_str().unwrap();
        let empty: &Vec<Value> = &Vec::new();
        let uids: &Vec<Value> = result["uids"].as_array().unwrap_or(empty);
        for an_uid in uids.iter() {
            let uid = an_uid["uid"].as_str().unwrap();
            let blockstamp = an_uid["meta"]["timestamp"].as_str().unwrap();
            let signature = an_uid["self"].as_str().unwrap();
            let revoked = an_uid["revoked"].as_bool().unwrap();
            identities.push(LookupIdentity::new(pubkey, uid, blockstamp, signature,revoked));
        }
    }
    identities
}