use std::fmt::{Display, Formatter};

use serde::{Deserialize, Serialize};

use crate::bma::BmaNode;

#[derive(Serialize, Deserialize)]
struct BmaCertification {
    cert: String
}

#[derive(Serialize, Deserialize)]
struct BmaMembership {
    membership: String
}

impl Display for BmaCertification {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str(self.cert.as_str())
    }
}

pub fn certify(node: &BmaNode, idty: &String, sig: &String) -> Result<(), String> {
    let cert = BmaCertification { cert: format!("{}{}\n", idty, sig) };
    let address = node.get_address();
    let client = reqwest::blocking::Client::new();
    let resp = client.post(format!("{}/wot/certify", address))
        .json(&cert)
        .send()
        .expect("Error during POST /wot/certify");

    if resp.status() != 200 {
        return Err(format!("KO status : {}, message: {}", resp.status(), resp.text().unwrap()));
    }
    Ok(())
}

pub fn membership(node: &BmaNode, idty: &String, sig: &String) -> Result<(), String> {
    let ms = BmaMembership { membership: format!("{}{}\n", idty, sig) };
    let address = node.get_address();
    let client = reqwest::blocking::Client::new();
    let resp = client.post(format!("{}/blockchain/membership", address))
        .json(&ms)
        .send()
        .expect("Error during POST /blockchain/membership");

    if resp.status() != 200 {
        return Err(format!("KO status : {}, message: {}", resp.status(), resp.text().unwrap()));
    }
    Ok(())
}