use std::env;
use std::process::exit;

use duniter_mini_client::cli::Command;

fn main() {
    let command = Command::from(env::args());

    let command = match command {
        Some(c) => c,
        None => {
            eprintln!("No command found");
            exit(1);
        }
    };

    if let Err(e) = command.execute() {
        eprintln!("Command executed with error: {}", e);
        exit(2);
    }
}