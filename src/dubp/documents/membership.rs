#[derive(Debug)]
pub struct Membership {
    pub version: u32,
    pub currency: String,
    pub issuer: String,
    pub idty_uid: String,
    pub idty_blockstamp: String,
    pub adhere_blockstamp: String,
}

impl crate::dubp::signable::Signable for Membership {
    fn to_signable(self: &Self) -> String {
        format!("Version: {}
Type: Membership
Currency: {}
Issuer: {}
Block: {}
Membership: IN
UserID: {}
CertTS: {}
", self.version, self.currency, self.issuer, self.adhere_blockstamp, self.idty_uid, self.idty_blockstamp)
    }
}