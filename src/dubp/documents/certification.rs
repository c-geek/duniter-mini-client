#[derive(Debug)]
pub struct Certification {
    pub version: u32,
    pub currency: String,
    pub issuer: String,
    pub idty_issuer: String,
    pub idty_uid: String,
    pub idty_blockstamp: String,
    pub idty_sig: String,
    pub cert_blockstamp: String,
}

impl crate::dubp::signable::Signable for Certification {
    fn to_signable(self: &Self) -> String {
        format!("Version: {}
Type: Certification
Currency: {}
Issuer: {}
IdtyIssuer: {}
IdtyUniqueID: {}
IdtyTimestamp: {}
IdtySignature: {}
CertTimestamp: {}
", self.version, self.currency, self.issuer, self.idty_issuer, self.idty_uid, self.idty_blockstamp,
                self.idty_sig, self.cert_blockstamp)
    }
}