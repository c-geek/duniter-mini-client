pub mod documents;

pub mod signable;

#[cfg(test)]
mod tests {
    use crate::dubp::documents::certification::Certification;
    use crate::dubp::signable::Signable;

    #[test]
    fn test_signable_cert() {
        assert_eq!("Version: 10
Type: Certification
Currency: g1-test
Issuer: 3dnbnYY9i2bHMQUGyFp5GVvJ2wBkVpus31cDJA5cfRpj
IdtyIssuer: 39YyHCMQNmXY7NkPCXXfzpV1vYct4GBxwgfyd4d72HmB
IdtyUniqueID: cgeek-4
IdtyTimestamp: 542574-000574E54C2E53E74304035220A5335B421C1389FC74942C0E3A7F732B4E6CC0
IdtySignature: vnbHBdlobFbxatrg02oJPTwWlHdbeX+UlaPMHuT28mtrYYitSewKqjdhb3Fis+j2NU+r1AQJ0LzV0f/tfFk+Aw==
CertTimestamp: 821468-000325F5067B01F46FD21FCCEB8BE996D6AE9A22D7AD26F402EA263257CA0E0F
", Certification {
            version: 10,
            currency: "g1-test".to_string(),
            issuer: "3dnbnYY9i2bHMQUGyFp5GVvJ2wBkVpus31cDJA5cfRpj".to_string(),
            idty_issuer: "39YyHCMQNmXY7NkPCXXfzpV1vYct4GBxwgfyd4d72HmB".to_string(),
            idty_uid: "cgeek-4".to_string(),
            idty_blockstamp: "542574-000574E54C2E53E74304035220A5335B421C1389FC74942C0E3A7F732B4E6CC0".to_string(),
            idty_sig: "vnbHBdlobFbxatrg02oJPTwWlHdbeX+UlaPMHuT28mtrYYitSewKqjdhb3Fis+j2NU+r1AQJ0LzV0f/tfFk+Aw==".to_string(),
            cert_blockstamp: "821468-000325F5067B01F46FD21FCCEB8BE996D6AE9A22D7AD26F402EA263257CA0E0F".to_string(),
        }.to_signable());
    }
}