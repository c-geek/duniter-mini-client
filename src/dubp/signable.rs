pub trait Signable {
    fn to_signable(self: &Self) -> String;
}
